from django.shortcuts import render, get_object_or_404

# Create your views here.

from . import models

print("Los geht es hier: http://localhost:8000/textadventure/start")

def station(request, name="start"):
    '''
    Hier wird die passende Station geholt und ausgegeben.
    Wird kein Name übergeben, dann wird die Station 'start' geholt.
    '''
    station = get_object_or_404(models.Station, name=name) 
    return render(request, "textadventure/station.html", {"station": station})
